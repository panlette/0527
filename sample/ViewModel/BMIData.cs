﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sample.ViewModel
{
    public class BMIData
    {
        [Required(ErrorMessage ="required filed")]
        [Range(30,300,ErrorMessage ="30~300")]
        public float weight { get; set; }
        [Required(ErrorMessage = "required filed")]
        [Range(50, 200, ErrorMessage = "50~200")]
        public float height { get; set; }
        public float BMI { get; set; }
        public string Level { get; set; }
    }
}