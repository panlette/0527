﻿using sample.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sample.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if (ModelState.IsValid)
            {


                if (data.height < 50 || data.height > 200)
                {
                    ViewBag.herror = "50~200";
                }
                if (data.weight < 30 || data.weight > 300)
                {
                    ViewBag.werror = "30~300";
                }

                float m_hh = data.height / 100;
                float bmi = data.weight / (m_hh * m_hh);

                string level = "";
                if (bmi < 18.5)
                {
                    level = "太瘦";
                }
                else if (bmi > 18.5 && bmi < 24)
                {
                    level = "適中";
                }
                else if (bmi > 35)
                {
                    level = "太胖";
                }

                data.BMI = bmi;
                data.Level = level;
            }
            return View(data);
        }
    }
}