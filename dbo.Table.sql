﻿CREATE TABLE [dbo].[Users]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(10) NULL, 
    [Email] NVARCHAR(50) NULL, 
    [Password] NVARCHAR(10) NULL, 
    [Birthday] DATETIME2 NULL, 
    [Gender] BIT NULL
)
